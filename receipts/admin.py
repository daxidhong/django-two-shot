from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt

# Register your models here.

# use pass because we have more than 1 app
# two shot, we have account, expense, and receipt
# in scrumptious we use list display because its the only admin in the entire project


@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    pass


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    pass
